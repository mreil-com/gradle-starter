package com.mreil.gradleplugins.example

import org.gradle.api.Plugin
import org.gradle.api.Project

class ExamplePlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.extensions.create(ExampleExtension.NAME, ExampleExtension, project)

        project.tasks.create("example", ExampleTask)
    }
}
