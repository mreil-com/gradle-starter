package com.mreil.gradleplugins.example

import org.gradle.api.DefaultTask
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

class ExampleTask extends DefaultTask {
    @TaskAction
    def task() {
        ExampleExtension extension =
                project.extensions.getByName(ExampleExtension.NAME) as ExampleExtension

        getProject().getLogger().log(LogLevel.WARN, "MEH")
    }
}
