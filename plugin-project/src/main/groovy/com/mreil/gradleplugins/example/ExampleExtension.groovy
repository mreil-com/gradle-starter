package com.mreil.gradleplugins.example

import org.gradle.api.Project

class ExampleExtension {
    public static final String NAME = "example"
    Project project

    ExampleExtension(Project project) {
        this.project = project
    }
}
