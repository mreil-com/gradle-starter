package com.mreil.gradleplugins.example

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

/**
 * Functional test using the GradleRunner
 */
import static org.gradle.util.GFileUtils.writeFile

class ExamplePluginFuncTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    File buildFile

    @Shared
    GradleRunner gradleRunner

    @Before
    def setupGradleProject() throws IOException {
        buildFile = testProjectDir.newFile("build.gradle")

        String buildFileContent = '''\
plugins {
    id 'com.mreil.example'
}
'''

        writeFile(buildFileContent, buildFile)

        gradleRunner = GradleRunner.create()
                .withPluginClasspath()
                .withProjectDir(testProjectDir.getRoot())

    }

    def "test plugin"() {
        when: "something"
        BuildResult result = gradleRunner.withArguments("example")
                .build()

        then: "I can do things"
        result.output.contains("MEH")
        result.task(":example").getOutcome() == TaskOutcome.SUCCESS
    }
}
