package com.mreil.gradleplugins.example

import org.gradle.tooling.GradleConnector
import org.gradle.tooling.ProjectConnection
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

/**
 * Plugin test using the Tooling API
 */
class ExamplePluginTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    ProjectConnection connection

    @Before
    def setupGradleProject() {
        connection = GradleConnector.newConnector()
                .forProjectDirectory(testProjectDir.root)
                .connect()
    }

    def "test plugin"() {
        when: "I run a thing"
        connection.newBuild().forTasks("tasks").run()

        then:
        1 == 1
    }
}
