# gradle-starter

[![Download][bintray-badge]][bintray-link]
[![Coverage][codecov-badge]][codecov]
[![License][license-badge]][license]
[![standard-readme compliant][readme-badge]][readme-link]

> Gradle starter project

Start a [Gradle][gradle] project quickly.


## Install

Clone the repository.


## Usage

* Rename the project in [settings.gradle](settings.gradle).
* Remove the included modules from [settings.gradle](settings.gradle) and
    delete the directories.
* Update the [README](README.md). Note that the current README follows the [Standard Readme Style](https://github.com/RichardLitt/standard-readme).
    Try to stick to this format.
* Remove `config/.gitignore`
* Check for "__TODO__" in code and comments.

### Conventions


#### Project Version

The project version is configured in [gradle.properties](gradle.properties) instead of the build file.


### Available Modules

Modules can be disabled by removing their inclusion in [build.gradle](build.gradle). E.g. the __java__ module
can be disabled by commenting out the following line:

```
apply from: 'build-java.gradle'
``` 


#### java

For all projects using the `java` plugin:

* Enable the [Dependency management plugin][dm-plugin]
* Define some standard dependencies (e.g. [SLF4J][slf4j])
* Define versions for some standard dependencies
* Use [Spock][spock] for testing
* Enable the JUnit test report


#### java-coverage

For all projects using the `java` plugin:

* Enable code coverage collection when running tests using [JaCoCo][jacoco]
* Uses the [Gradle JaCoCo plugin][jacocoplugin]
* Run `./gradle jacocoTestReport` to generate the report


#### java-checkstyle

For all projects using the `java` plugin:

* Enable [checkstyle][checkstyle] checking
* Uses the [Gradle Checkstyle plugin][checkstyle-plugin]
* Run `./gradle check` to generate the report
* If your project does not have a checkstyle configuration file, one is automatically downloaded and added to the `config` directory.


#### java-findbugs

For all projects using the `java` plugin:

* Enable [FindBugs][findbugs] checking
* Uses the [Gradle FindBugs plugin][findbugs-plugin]
* Run `./gradle check` to generate the report


#### java-maven

For all projects using the `java` and `maven-publish` plugins:

* Enable publishing to the local Maven repo
* Uses the [Gradle maven-publish plugin][maven-publish-plugin]
* Run `./gradle publish` to publish


#### groovy-codenarc

For all projects using the `groovy` plugin:

* Enable [CodeNarc][codenarc] checking
* Uses the [Gradle CodeNarc plugin][codenarc-plugin]
* Run `./gradle check` to generate the report
* If your project does not have a CodeNarc configuration file, one is automatically downloaded and added to the `config` directory.


#### gradle-plugin

For all projects using the `java-gradle-plugin` plugin:

* Enable [Gradle TestKit][testkit]


## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]

[readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[readme-link]: https://github.com/RichardLitt/standard-readme
[bintray-badge]: https://api.bintray.com/packages/mreil/test/gradle-starter-java-project/images/download.svg
[bintray-link]: https://bintray.com/mreil/test/gradle-starter-java-project
[codecov-badge]: https://codecov.io/bb/XXX/XXX/branch/master/graph/badge.svg
[codecov]: https://codecov.io/bb/XXX/XXX
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license]: LICENSE
[changelog]: CHANGELOG.md
[gradle]: https://gradle.org/  "gradle.org"
[dm-plugin]: https://github.com/spring-gradle-plugins/dependency-management-plugin  "Dependency Management Plugin"
[slf4j]: https://www.slf4j.org/ "SLF4J"
[spock]: http://spockframework.org/ "Spock"
[jacoco]: http://www.eclemma.org/jacoco/ "JaCoCo"
[jacocoplugin]: https://docs.gradle.org/current/userguide/jacoco_plugin.html "JaCoCo Plugin"
[checkstyle]: http://checkstyle.sourceforge.net/ "checkstyle"
[checkstyle-plugin]: https://docs.gradle.org/current/userguide/checkstyle_plugin.html "Checkstyle plugin"
[findbugs]: http://findbugs.sourceforge.net/ "FindBugs"
[findbugs-plugin]: https://docs.gradle.org/current/userguide/findbugs_plugin.html "FindBugs plugin" 
[codenarc]: http://codenarc.sourceforge.net/ "CodeNarc"
[codenarc-plugin]: https://docs.gradle.org/current/userguide/codenarc_plugin.html "CodeNarc plugin"
[maven-publish-plugin]: https://docs.gradle.org/current/userguide/publishing_maven.html "Maven Publishing"
[testkit]: https://docs.gradle.org/current/userguide/test_kit.html "Gradle TestKit"
