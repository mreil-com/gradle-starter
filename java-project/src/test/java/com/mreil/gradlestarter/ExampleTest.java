package com.mreil.gradlestarter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ExampleTest {
    @Test
    @DisplayName("Example test")
    void test() {
        Example ex = new Example();

        ex.doThings();
    }
}
