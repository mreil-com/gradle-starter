package com.mreil.gradlestarter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Example {
    public void doThings() {
        log.info("Hello!");
    }
}
